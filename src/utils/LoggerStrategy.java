package utils;

/**
 * Interface for strategy pattern used with logger.
 */
public interface LoggerStrategy {
    /**
     * Define the strategy to use for writing a log message into a stream.
     * @param message The message to write into the stream.
     */
    void writeToStream(final String message);
}
