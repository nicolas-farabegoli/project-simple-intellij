package utils;

public class Logger {

    private static Logger instance = null;

    private Logger() { }

    synchronized public static Logger getInstance() {
        if (instance == null) {
            instance = new Logger();
        }
        return instance;
    }

    public void logPrintString(LoggerMode mode, String s, LoggerStrategy strategy){
        printLog(strategy, mode.getValue() + " " + s);
    }

    public void logPrintStrings(LoggerMode mode, Iterable<String> array, LoggerStrategy strategy){
        array.forEach(e -> printLog(strategy, mode.getValue() + " " + e));
    }

    private void printLog(LoggerStrategy strategy, String message) {
        strategy.writeToStream(message);
    }

    public enum LoggerMode {
        DEBUG("[DEBUG]"),
        INFO("[INFO]"),
        WARNING("[WARNING]"),
        ERROR("[ERROR]");

        private String value;

        public String getValue() {
            return value;
        }

        LoggerMode(String value) {
            this.value =  value;
        }
    }
}
