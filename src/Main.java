import model.Person;
import utils.Logger;
import utils.Logger.LoggerMode;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args){
        Logger l = Logger.getInstance();
        Person p = new Person("Bob", 33);
        
        l.logPrintString(LoggerMode.INFO, "Hello", System.out::println);
        List<String> logList = Arrays.asList("LOG1", "LOG2", "LOG3");
        l.logPrintStrings(LoggerMode.ERROR, logList, System.err::println);
    }
}
